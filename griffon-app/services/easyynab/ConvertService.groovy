package easyynab

import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

class ConvertService {

	BigDecimal parseValue(String value, char decimalSeparator, char groupingSeparator) {
		def dfs = new DecimalFormatSymbols(decimalSeparator: decimalSeparator, groupingSeparator: groupingSeparator)
		new DecimalFormat(decimalFormatSymbols: dfs, parseBigDecimal: true).parse(value.replaceAll('\\+', ''))
	}

	String decode(String str, char unknownCh) {
		StringBuffer sb = new StringBuffer()
		int i1
		int i2 = 0

		while (i2 < str.length()) {
			i1 = str.indexOf("&#", i2)
			if (i1 == -1) {
				sb.append(str.substring(i2))
				break
			}
			sb.append(str.substring(i2, i1))
			i2 = str.indexOf(";", i1)
			if (i2 == -1) {
				sb.append(str.substring(i1))
				break
			}

			String tok = str.substring(i1 + 2, i2)
			try {
				int radix = 10
				if (tok.charAt(0) == 'x' || tok.charAt(0) == 'X') {
					radix = 16
					tok = tok.substring(1)
				}
				sb.append((char) Integer.parseInt(tok, radix))
			} catch (NumberFormatException exp) {
				sb.append(unknownCh)
			}
			i2++
		}
		return sb.toString()
	}
}