package easyynab

convertAction = action(closure: controller.convertFile, name: "Convert")
fileViewerFrame = application(title: 'EasyYNAB',
		preferredSize: [400, 200],
		pack: true,
		locationByPlatform: true,
		iconImage: imageIcon('/griffon-icon-48x48.png').image,
		iconImages: [imageIcon('/griffon-icon-48x48.png').image,
				imageIcon('/griffon-icon-32x32.png').image,
				imageIcon('/griffon-icon-16x16.png').image]) {

			migLayout(layoutConstraints: 'fill')

			label(text: 'Select file to convert:', constraints: 'span 2, growx, wrap')
			textField(columns: 20, text: bind('fileName', target: model, id: 'textBinding'), constraints: 'left')
			button("...", actionPerformed: controller.browse, constraints: 'wrap')
			label(text: 'Select start date:', constraints: 'left')
			textField(columns: 20, text: bind('startDate', source: model, mutual: true), constraints: 'right, wrap')
			button(convertAction, constraints: 'span 2, growx')
		}

fileChooserWindow = fileChooser()