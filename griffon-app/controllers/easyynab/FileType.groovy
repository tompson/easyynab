package easyynab

public enum FileType {

	RAIFFEISEN(2, 3, 1), CREDITCARD(1, 6, 2)

	int date
	int value
	int description

	private FileType(int date, int value, int description) {
		this.date = date
		this.value = value
		this.description = description
	}
}