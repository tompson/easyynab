package easyynab

import javax.swing.*

class EasyYNABController {
	// these will be injected by Griffon
	def model
	def view

	def convertService

	def convertFile = { evt = null ->
		def out = new File(model.fileName + '-ynab.csv').newWriter('utf-8')
		def startDate = new Date().parse('dd.MM.yyyy', model.startDate)
		out << 'Date,Payee,Category,Memo,Outflow,Inflow' << "\n"

		def fileType = FileType.RAIFFEISEN
		if (model.fileName.contains('finstatus')) {
			fileType = FileType.CREDITCARD
		}

		convertService.decode(new File(model.fileName).getText('iso-8859-1'), (char) 'X').readLines().each { String line ->
			if (fileType == FileType.CREDITCARD) {
				line = line.replaceAll('"', '')
			}
			def values = line.split(";")
			def date = new Date().parse('dd.MM.yyyy', values[fileType.date])
			def description = values[fileType.description].split("\\|")[0].replaceAll(',', ' ')
			def amount = convertService.parseValue(values[fileType.value], (char) ',', (char) '.')
			if (date >= startDate) {
				out << [date.format('dd/MM/yyyy'), description, '', description, (amount * -1)].join(",") << "\n"
			}
		}
		out.close()
	}

	def browse = { evt = null ->
		def openResult = view.fileChooserWindow.showOpenDialog(view.fileViewerFrame)
		if (JFileChooser.APPROVE_OPTION == openResult) {
			model.fileName = view.fileChooserWindow.selectedFile.toString()
			view.textBinding.reverseUpdate()
		}
	}


}
