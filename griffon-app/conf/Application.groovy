application {
    title = 'EasyYNAB'
    startupGroups = ['easyYNAB']

    // Should Griffon exit when no Griffon created frames are showing?
    autoShutdown = true

    // If you want some non-standard application class, apply it here
    //frameClass = 'javax.swing.JFrame'
}
mvcGroups {
    // MVC Group for "easyYNAB"
    'easyYNAB' {
        model      = 'easyynab.EasyYNABModel'
        view       = 'easyynab.EasyYNABView'
        controller = 'easyynab.EasyYNABController'
    }

}
